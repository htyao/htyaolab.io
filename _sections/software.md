---
title: Software 
icon: fa-screwdriver-wrench
order: 4
---

{% for software in site.data.software %}
<div class="software">
<a href="{{ software.link }}">
<div class="row justify-content-md-center">
  <div class="col-md-10 software-left">
    <div class="col-12 col-md-9 software-body">
      <h4 class="media-heading">{{ software.name }}</h4>
      <div class="trajectory-description">
        {{ software.abstract }}
      </div>
    </div>
    <div class="col-6 col-md-2 institution-logo software-logo">
      <img class="software-img" src="{{ software.logo }}" alt="{{ software.name }}">
    </div>
  </div>
</div>
</a>
</div>
{% endfor %}


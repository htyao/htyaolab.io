---
title: Intro
auto-header: none
icon: fa-comment
center: yes
order: 1
---

## Hua-Ting <span style="color:orange">Yao</span>
#### Post-Doc

[TBI](https://www.tbi.univie.ac.at/) @ University of Vienna 
<!-- [AMIBio](https://www.lix.polytechnique.fr/amibio/)@École Polytechnique -->  
<!-- [Waldispühl group](http://csb.cs.mcgill.ca/)@McGill University -->

<a href="https://orcid.org/0000-0002-1720-5737" title="ORCID">
  <span class="fa-stack" style="vertical-align: top;">
    <i class="ai ai-orcid ai-stack-2x"></i>
    </span>
</a>
<a href="https://scholar.google.com/citations?user=28CZVCUAAAAJ" title="Google Scholar">
  <span class="fa-stack" style="vertical-align: top;">
    <i class="fas fa-circle fa-stack-2x"></i>
    <i class="ai ai-google-scholar ai-stack-2x ai-inverse"></i>
  </span>
</a>
<a href="{{site.cv}}" title="Download Curriculum Vitae">
  <span class="fa-stack" style="vertical-align: top;">
    <i class="fas fa-circle fa-stack-2x"></i>
    <i class="ai ai-cv ai-stack-2x ai-inverse"></i>
  </span>
</a>


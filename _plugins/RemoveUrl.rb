# Remove url from citation


module Jekyll
  module RemoveUrl
    def removeurl(text)
      # text.gsub!(/[^[:print:]]/i, '')
      text.gsub!(/#{URI::regexp(['http', 'https'])}/, '')
    end
  end
end


Liquid::Template.register_filter(Jekyll::RemoveUrl)
